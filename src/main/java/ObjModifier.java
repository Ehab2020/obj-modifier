import java.nio.FloatBuffer;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here
		
		// Maximum and Minimum values on the coordinates system
		float boundsMaxX = Float.MAX_VALUE;
		float boundsMinX = -Float.MAX_VALUE;
		
		float boundsMaxY = Float.MAX_VALUE;
		float boundsMinY = -Float.MAX_VALUE;

		float boundsMaxZ = Float.MAX_VALUE;
		float boundsMinZ = -Float.MAX_VALUE;
		
		// Determining the coordinates of the minimum and maximum points of the object according to coordinates system
		for(int i = 0; i +2 < buf.capacity();i++)
		{
			boundsMinX = GetMinimum(buf.get(i), boundsMaxX);
			boundsMaxX = GetMaximum(buf.get(i), boundsMinX);
			i++;
			boundsMinY = GetMinimum(buf.get(i), boundsMaxY);
			boundsMaxY = GetMaximum(buf.get(i), boundsMinY);
			i++;
			boundsMinZ = GetMinimum(buf.get(i), boundsMaxZ);
			boundsMaxZ = GetMaximum(buf.get(i), boundsMinZ);
		}
		
		// Get shifting point coordinates
		float shiftPointX = (boundsMaxX + boundsMinX)/2;
		float shiftPointY = (boundsMaxY + boundsMinY)/2;
		float shiftPointZ = (boundsMaxZ + boundsMinZ)/2;
		
		// Shifting points to origin of coordinates system
		for(int i = 0; i+2 < buf.capacity();i++)
		{
			buf.put(i,buf.get(i) - shiftPointX); 
			buf.put(+i, buf.get(i) - shiftPointY);
			buf.put(++i, buf.get(i) - shiftPointZ);
		}	
	}
	
	// Compare two values and return the lower one
	private static float GetMinimum(float v1, float v2)
	{
		return v1 < v2? v1 : v2;
	}

	// Compare  two values and return the bigger one
	private static float GetMaximum(float v1, float v2)
	{
		return v1 > v2? v1 : v2;
	}
}